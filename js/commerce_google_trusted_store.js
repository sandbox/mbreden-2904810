(function($) {
    Drupal.behaviors.commerceGoogleTrustedStore = {
        attach: function(context, settings) {

            // Call this only once per page load.
            $('body', context).once('commerceGoogleTrustedStore', function() {
                // Global variable for Google.
                gts = [];

                gts.push(["id", Drupal.settings.commerceGoogleTrustedStore.trustedStoreId]);
                gts.push(["badge_position", Drupal.settings.commerceGoogleTrustedStore.badgePosition]);
                gts.push(["locale", Drupal.settings.commerceGoogleTrustedStore.pageLanguage]);
                gts.push(["google_base_offer_id", Drupal.settings.commerceGoogleTrustedStore.productId]);
                gts.push(["google_base_subaccount_id", Drupal.settings.commerceGoogleTrustedStore.shoppingAccountId]);

                // If the container id is provided, add it.
                if (Drupal.settings.commerceGoogleTrustedStore.gtsContainerId && Drupal.settings.commerceGoogleTrustedStore.badgePosition == "USER_DEFINED") {
                    gts.push(["badge_container", Drupal.settings.commerceGoogleTrustedStore.gtsContainerId]);
                }

                (function () {
                    var gts = document.createElement("script");
                    gts.type = "text/javascript";
                    gts.async = true;
                    gts.src = "https://www.googlecommerce.com/trustedstores/api/js";
                    var s = document.getElementsByTagName("script")[0];
                    s.parentNode.insertBefore(gts, s);
                })();
            })
        }
    }
}(jQuery));