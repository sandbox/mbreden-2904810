<?php

/**
 * @file
 * commerce_google_trusted_store.admin.inc
 */

/**
 * Callback for the settings form.
 */
function commerce_google_trusted_store_settings_form($form, &$form_state) {
  // Need to get a list of countries.
  include_once DRUPAL_ROOT . '/includes/locale.inc';

  $form['commerce_google_trusted_store_trusted_store_id'] = array(
    '#type' => 'textfield',
    '#title' => t('Trusted Store ID'),
    '#description' => t('The Google trusted store ID.'),
    '#default_value' => variable_get('commerce_google_trusted_store_trusted_store_id', ''),
    '#required' => TRUE,
  );

  $form['commerce_google_trusted_store_shopping_account_id'] = array(
    '#type' => 'textfield',
    '#title' => t('Shopping Account ID'),
    '#description' => t('The Google shopping account merchant ID.'),
    '#default_value' => variable_get('commerce_google_trusted_store_shopping_account_id', ''),
    '#required' => TRUE,
  );

  $form['commerce_google_trusted_store_locale'] = array(
    '#type' => 'textfield',
    '#title' => t('Locale'),
    '#description' => t('The locale should be in the format of language_COUNTRY ie en_US'),
    '#default_value' => variable_get('commerce_google_trusted_store_locale', ''),
    '#required' => TRUE,
  );

  $form['commerce_google_trusted_store_shipping_days'] = array(
    '#type' => 'textfield',
    '#title' => t('Shipping Time'),
    '#element_validate' => array('element_validate_integer_positive'),
    '#description' => t('How many days it usually takes for an order to ship once completed. ie 2'),
    '#default_value' => variable_get('commerce_google_trusted_store_shipping_days', ''),
    '#required' => TRUE,
  );

  $form['commerce_google_trusted_store_delivery_days'] = array(
    '#type' => 'textfield',
    '#title' => t('Delivery Time'),
    '#element_validate' => array('element_validate_integer_positive'),
    '#description' => t('How many days it usually takes for an order to arrive once shipped. ie 10'),
    '#default_value' => variable_get('commerce_google_trusted_store_delivery_days', ''),
    '#required' => TRUE,
  );

  $form['commerce_google_trusted_store_product_id'] = array(
    '#type' => 'radios',
    '#title' => t('Type of Product ID'),
    '#description' => t('Which property to grab for the "Product ID" part of GTS.'),
    '#options' => array(
      'VARIATION_SKU' => t('Variation SKU'),
      'VARIATION_ID' => t('Variation ID'),
    ),
    '#default_value' => variable_get('commerce_google_trusted_store_product_id', 'VARIATION_SKU'),
  );

  $form['commerce_google_trusted_store_badge_position'] = array(
    '#type' => 'radios',
    '#title' => t('Badge Location'),
    '#description' => t('The position of the badge.'),
    '#options' => array(
      'BOTTOM_RIGHT' => t('Bottom Right'),
      'BOTTOM_LEFT' => t('Bottom Left'),
      'USER_DEFINED' => t('User Defined'),
    ),
    '#default_value' => variable_get('commerce_google_trusted_store_badge_position', 'BOTTOM_RIGHT'),
  );

  $form['commerce_google_trusted_store_gts_container_id'] = array(
    '#type' => 'textfield',
    '#title' => t('Badge Container &lt;div&gt; ID'),
    '#description' => t('The container &lt;div&gt; ID for the badge (ie MY_BADGE_DIV for a div that is &lt;div id="MY_BADGE_DIV"&gt;)'),
    '#default_value' => variable_get('commerce_google_trusted_store_gts_container_id', ''),
    '#states' => array(
      'enabled' => array(
        ':input[name="commerce_google_trusted_store_badge_position"]' => array('value' => 'USER_DEFINED'),
      ),
      'visible' => array(
        ':input[name="commerce_google_trusted_store_badge_position"]' => array('value' => 'USER_DEFINED'),
      ),
    ),
  );

  return system_settings_form($form);
}
